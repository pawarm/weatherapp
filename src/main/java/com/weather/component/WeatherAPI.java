package com.weather.component;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Class to connect and get weather information from weather api
 * 
 * @author pawarm
 *
 */
@Component("weather")
public class WeatherAPI {
	
	private static final Logger logger = LoggerFactory.getLogger(WeatherAPI.class);
	
	@Value("${url:https://weather.api.here.com/weather/1.0/report.json}")
	private String url;
	
	@Value("${app_id:68yqvd4svKy6647IM77P}")
	private String app_id;
	
	@Value("${app_code:Mg8TwkZ17UEA0QfL5enFNw}")
	private String app_code;
	
	@Value("${product:forecast_hourly}")
	private String product;
	
	@Value("${metric:false}")
	private boolean metric;

	@Autowired
	private RestTemplate restTemplate;
	
	/**
	 * This method is used to connect to weather api based on provided zip code
	 * 
	 * @param zipCode
	 * @return 
	 */
	public ResponseEntity<?> getWeatherInfoByZipCode(int zipCode) {
		logger.info("Provided zip code: " + zipCode);

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
				.queryParam("app_id", app_id).queryParam("app_code", app_code)
				.queryParam("product", product).queryParam("metric", metric).queryParam("zipcode", zipCode);

		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		try {
			ResponseEntity<?> response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, requestEntity,
					String.class);
			return response;
		} catch (Exception e) {
			logger.error("Exception: "+e.getMessage());
			return null;
		}
	}
}
