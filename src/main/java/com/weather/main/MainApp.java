package com.weather.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.weather.component.WeatherAPI;
import com.weather.util.ParserUtil;

/**
 * Main class of the Application
 * 
 * @author pawarm
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.weather")
public class MainApp {
	
	private static final Logger logger = LoggerFactory.getLogger(MainApp.class);

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(MainApp.class, args);
		WeatherAPI weather = (WeatherAPI) context.getBean("weather");
		System.out.print("Enter the 5 digit Zip code of the city in US: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String input;
		try {
			while ((input = br.readLine()) != null && !input.equals("exit") && !input.equals("quit")) {

				int zipCode = Integer.parseInt(input);
				ResponseEntity<?> response = weather.getWeatherInfoByZipCode(zipCode);
				if (response.getStatusCode() == HttpStatus.OK) {
					ParserUtil.parseWeatherResponse(response.getBody().toString());
				} else {
					logger.info("Unable to connect to weather api.");
				}				
				
				System.out.print(
						"\nWants to check temperature at another location,\nthen please Enter the 5 digit Zip code of the city in USA or enter exit to quit: ");
			}
		} catch (NumberFormatException e) {
			logger.error("Please enter a valid zip code: "+e.getMessage());
		} catch (IOException e) {
			logger.error("Exception while reading user input: "+e.getMessage());
		}
		System.out.println(".....Exiting from Weather App.....");
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}