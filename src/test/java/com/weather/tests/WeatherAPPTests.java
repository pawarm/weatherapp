package com.weather.tests;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import com.weather.component.WeatherAPI;

@RunWith(SpringRunner.class)
public class WeatherAPPTests {

	@Mock
	private WeatherAPI weatherAPI;

	@Test
	public void testGetWeatherInfoByZipCode_Error() {
		try {
			weatherAPI.getWeatherInfoByZipCode(1234);
		} catch (Exception e) {
			assertThat(e.getMessage())
		      .isEqualTo("400 Bad Request");
		}
	}
	
	@Test
	public void testGetWeatherInfoByZipCode_Success() {
		weatherAPI.getWeatherInfoByZipCode(94040);
	}
}
